package com.rabotauaselenide.test;

import com.codeborne.selenide.Condition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static com.codeborne.selenide.Selenide.*;

public class UserActionsTest {

    @Before
    public void userLogin(){
        open("http://rabota.ua");
        login();
    }

    @After
    public void userLogout(){
        openHeaderLink("[href=\'javascript:void(0)\']");
        $("#Header_JobsearcherLoggedin_btnExit").click();
    }

    @Test
    //all actions after authorization
    public void sendResume(){
        openHeaderLink("[href=\'/\']"); //redirect to main page
        seachVacancy("QA Engineer");

    }

    @Test
    public void updateResume(){
        openHeaderLink("[href=\'/jobsearch/notepad/cvs\']");
        $("#centerZone_ResumeList_grVwResume_lnkRefresh_0").click();
    }

    public void seachVacancy(String vacancy){
        $(".rua-g-clearfix").click();
        $("#beforeContentZone_vacSearch_Keyword").setValue(vacancy).pressEnter();
    }
    public void login(){

        String PATH_TO_PROPERTIES="src/main/resources/config.properties";
        FileInputStream fileInputStream;
        Properties prop = new Properties();
        try{
            fileInputStream = new FileInputStream(PATH_TO_PROPERTIES);
            prop.load(fileInputStream);
        } catch (IOException e){
            System.out.println("File not found:" + PATH_TO_PROPERTIES);
            e.printStackTrace();
        }

        openHeaderLink("[href='/jobsearch/login']");
        $("#centerZone_ZoneLogin_txLogin").setValue(prop.getProperty("login"));
        $("#centerZone_ZoneLogin_txPassword").setValue(prop.getProperty("password"));
        $("#centerZone_ZoneLogin_btnLogin").click();
        openHeaderLink("[href=\'javascript:void(0)\']");
        $("#Header_JobsearcherLoggedin_btnExit").shouldBe(Condition.visible);
    }

    public void openHeaderLink(String link){
        $(link).click();

    }
}
